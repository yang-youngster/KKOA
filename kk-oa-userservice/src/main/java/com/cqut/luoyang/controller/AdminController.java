package com.cqut.luoyang.controller;

import com.cqut.luoyang.entity.TbUser;
import com.cqut.luoyang.service.AdminService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



/**
 * @BelongsProject: Microservice
 * @BelongsPackage: com.cqut.luoyang.controller
 * @Author: LuoYang
 * @CreateTime: 2024-06-04  10:16
 * @Description: TODO
 * @Version: 1.0
 */
@RequestMapping("/micro/demo")
@RestController
public class AdminController {
    @Autowired
    AdminService adminService;
    @GetMapping("/index/{id}")
    public TbUser index(@PathVariable Long id) {
      return adminService.selectByPrimaryKey(id);
     }
}
