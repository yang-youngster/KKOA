package com.cqut.luoyang.service.impl;

import com.cqut.luoyang.entity.TbUser;
import com.cqut.luoyang.mapper.TbUserMapper;
import com.cqut.luoyang.service.AdminService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @BelongsProject: Microservice
 * @BelongsPackage: com.cqut.luoyang.service.impl
 * @Author: LuoYang
 * @CreateTime: 2024-06-04  10:15
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class AdminServiceImpl implements AdminService {
    @Autowired
    TbUserMapper tbUserMapper;
    @Override
    public TbUser selectByPrimaryKey(Long id) {
        return tbUserMapper.selectByPrimaryKey(id);
    }
}
