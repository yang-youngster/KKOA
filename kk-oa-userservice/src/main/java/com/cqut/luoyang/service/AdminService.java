package com.cqut.luoyang.service;

import com.cqut.luoyang.entity.TbUser;

public interface AdminService {

    TbUser selectByPrimaryKey(Long id);
}
