package com.cqut.luoyang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @BelongsProject: Microservice
 * @BelongsPackage: com.cqut.luoyang
 * @Author: LuoYang
 * @CreateTime: 2024-06-03  15:22
 * @Description: 微服务测试
 * @Version: 1.0
 */

@SpringBootApplication
public class UserServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(UserServiceApplication.class,args);
    }
}
