package com.cqut.luoyang.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @BelongsProject: Microservice
 * @BelongsPackage: com.cqut.luoyang.config
 * @Author: LuoYang
 * @CreateTime: 2024-06-04  09:56
 * @Description: TODO
 * @Version: 1.0
 */
@Configuration
@MapperScan("com.cqut.luoyang.mapper")
public class MybatisConfig {
}
