package com.cqut.luoyang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang
 * @Author: LuoYang
 * @CreateTime: 2024-06-22  14:11
 * @Description: TODO
 * @Version: 1.0
 */

@SpringBootApplication
public class KkOaGetawayApplication {
    public static void main(String[] args) {
        SpringApplication.run(KkOaGetawayApplication.class);
    }
}
