package com.cqut.luoyang.mapper;

import com.cqut.luoyang.domain.WkAdminRoleMenu;
import com.cqut.luoyang.domain.WkAdminRoleMenuExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WkAdminRoleMenuMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    long countByExample(WkAdminRoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int deleteByExample(WkAdminRoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int insert(WkAdminRoleMenu row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int insertSelective(WkAdminRoleMenu row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    List<WkAdminRoleMenu> selectByExample(WkAdminRoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    WkAdminRoleMenu selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByExampleSelective(@Param("row") WkAdminRoleMenu row, @Param("example") WkAdminRoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByExample(@Param("row") WkAdminRoleMenu row, @Param("example") WkAdminRoleMenuExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByPrimaryKeySelective(WkAdminRoleMenu row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_role_menu
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByPrimaryKey(WkAdminRoleMenu row);
}