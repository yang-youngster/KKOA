package com.cqut.luoyang.mapper;

import com.cqut.luoyang.domain.WkAdminOfficialImg;
import com.cqut.luoyang.domain.WkAdminOfficialImgExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface WkAdminOfficialImgMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    long countByExample(WkAdminOfficialImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int deleteByExample(WkAdminOfficialImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int deleteByPrimaryKey(Integer officialImgId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int insert(WkAdminOfficialImg row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int insertSelective(WkAdminOfficialImg row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    List<WkAdminOfficialImg> selectByExample(WkAdminOfficialImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    WkAdminOfficialImg selectByPrimaryKey(Integer officialImgId);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByExampleSelective(@Param("row") WkAdminOfficialImg row, @Param("example") WkAdminOfficialImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByExample(@Param("row") WkAdminOfficialImg row, @Param("example") WkAdminOfficialImgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByPrimaryKeySelective(WkAdminOfficialImg row);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wk_admin_official_img
     *
     * @mbg.generated Thu Jul 04 11:25:30 CST 2024
     */
    int updateByPrimaryKey(WkAdminOfficialImg row);
}