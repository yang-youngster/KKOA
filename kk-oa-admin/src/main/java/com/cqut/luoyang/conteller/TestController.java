package com.cqut.luoyang.conteller;

import com.cqut.luoyang.domain.WkAdminUser;
import com.cqut.luoyang.service.AdminService;
import jakarta.annotation.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang.conteller
 * @Author: LuoYang
 * @CreateTime: 2024-07-04  14:01
 * @Description: TODO
 * @Version: 1.0
 */
@RestController
@RequestMapping("/test")
public class TestController {

    //admin 接口
    @Resource
    AdminService adminService;

    @GetMapping("/getUser")
    public String getUser(WkAdminUser admin){
        return adminService.saveAdmin(admin);
    }

    @GetMapping("/getList")
    public List<WkAdminUser> getList(){
        return adminService.getAllList();
    }
}
