package com.cqut.luoyang;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.kafka.KafkaAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang
 * @Author: LuoYang
 * @CreateTime: 2024-06-23  16:55
 * @Description: TODO
 * @Version: 1.0
 */
@SpringBootApplication
@MapperScan("com.cqut.luoyang.mapper")
public class KkOaAdminApplication {
    public static void main(String[] args) {
        SpringApplication.run(KkOaAdminApplication.class);
    }
}
