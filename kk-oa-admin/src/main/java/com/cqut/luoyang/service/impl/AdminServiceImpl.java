package com.cqut.luoyang.service.impl;

import com.cqut.luoyang.domain.WkAdminUser;
import com.cqut.luoyang.mapper.WkAdminUserMapper;
import com.cqut.luoyang.service.AdminService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang.service.impl
 * @Author: LuoYang
 * @CreateTime: 2024-07-04  13:52
 * @Description: TODO
 * @Version: 1.0
 */
@Service
public class AdminServiceImpl implements AdminService {

    @Autowired
    WkAdminUserMapper wkAdminUserMapper;
    @Override
    public String saveAdmin(WkAdminUser admin) {
        return wkAdminUserMapper.insert(admin) == 1 ? "添加成功" : "添加失败";
    }

    @Override
    public List<WkAdminUser> getAllList() {
        return wkAdminUserMapper.selectByExample(null);
    }
}
