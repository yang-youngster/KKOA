package com.cqut.luoyang.service;

import com.cqut.luoyang.domain.WkAdminUser;
import org.springframework.stereotype.Service;

import java.util.List;

public interface AdminService {

    String saveAdmin(WkAdminUser admin);

    List<WkAdminUser> getAllList();
}
