package com.cqut.luoyang.constant;
/**
 * @description: 错误码方法
 * @author: LuoYang
 * @date: 2024/7/4 16:32
 * @param:
 * @return:
 **/
public interface ErrorCodeMethod {
    // 错误码
    Long getCode();
    // 错误信息
    String getMessage();
}
