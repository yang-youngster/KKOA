package com.cqut.luoyang.constant;

import lombok.Data;

/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang.common
 * @Author: LuoYang
 * @CreateTime: 2024-07-04  16:29
 * @Description: TODO
 * @Version: 1.0
 */

public enum ResultCodes implements ErrorCodeMethod {
    SUCCESS(200L, "操作成功"),
    FAILED(500L, "操作失败"),
    ;
    @Override
    public Long getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
    private   Long code;
    private String message;
    ResultCodes(Long code, String message) {
        this.code = code;
        this.message = message;
    }
}
