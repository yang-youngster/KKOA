package com.cqut.luoyang.common;

import com.cqut.luoyang.constant.ResultCodes;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
/**
 * @BelongsProject: KK-OA
 * @BelongsPackage: com.cqut.luoyang.common
 * @Author: LuoYang
 * @CreateTime: 2024-07-04  16:13
 * @Description: TODO
 * @Version: 1.0
 */
@Data
public class CommonResult<T> {
    /**
     * 状态码
     */
    @ApiModelProperty(value = "状态码", name = "code", example = "200")
    private Long code;
    /**
     * 返回内容
     */
    @ApiModelProperty(value = "返回内容", name = "msg",example = "操作成功")
    private String msg;
    /**
     * 返回数据
     */
    private T data;
    public CommonResult() {
    }
    public CommonResult(Long code, String msg) {
        this.code = code;
        this.msg = msg;
    }
    public CommonResult(Long code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 成功返回结果,堆结果进行重载
     * @param data
     * @return
     * @param <T>
     */
    public static <T> CommonResult<T> success(T data) {
        return new CommonResult<T>(ResultCodes.SUCCESS.getCode(), ResultCodes.SUCCESS.getMessage(), data);
    }
    public static <T> CommonResult<T> success(String msg, T data) {
        return new CommonResult<T>(ResultCodes.SUCCESS.getCode(), msg, data);
    }
    public static <T> CommonResult<T> success(String msg) {
        return new CommonResult<T>(ResultCodes.SUCCESS.getCode(), msg);
    }
    /**
     * 失败返回结果,堆结果进行重载
     * @param data
     * @return
     * @param <T>
     */
    public static <T> CommonResult<T> fail(T data) {
        return new CommonResult<T>(ResultCodes.FAILED.getCode(),ResultCodes.FAILED.getMessage(),data);
    }
    public static <T> CommonResult<T> fail(String msg, T data) {
        return new CommonResult<T>(ResultCodes.FAILED.getCode(), msg, data);
    }


}
